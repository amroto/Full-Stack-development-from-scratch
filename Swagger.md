# Swagger
[swagger OpenSource tools](https://swagger.io/docs/open-source-tools/swagger-editor/) are tools which helps you describe your API using what is called **OpenAPI**
and they include:
- [UI](https://swagger.io/tools/swagger-ui/)
- [Editor](https://swagger.io/tools/swagger-editor/)
- [CodeGen](https://swagger.io/tools/swagger-codegen/)
   
## what is OpenApi?
___
OpenAPI Specification is an API description format which allows you to descripe [REST APIs](https://www.restapitutorial.com) intire lifeCycle including (Endpoints,operations,Authentication,contact information, license,...etc) 
## why use OpenAPI?
___
The ability of APIs to describe their own structure is the root of all awesomeness in OpenAPI. Once written, an OpenAPI specification and Swagger tools can drive your API development further in various ways  
OpenAPI Specification are written in [JSON](https://www.json.org/) or [YAML](https://learn.getgrav.org/advanced/yaml) we will be using YAML as it is visually easier to look at and it has the ability to reference other items so it can handle relational information.  
you can read more about Basic Structure of OpenAPI from Swagger [Documentation]((https://swagger.io/docs/specification/basic-structure/))
## Getting Started
___
You can get starting By [Signing Up](https://app.swaggerhub.com/signup?utm_source=aw&utm_medium=ppcs&utm_campaign=brand-swaggerhub&utm_content=trial&utm_term=swaggerhub&gclid=CjwKCAjwhLHaBRAGEiwAHCgG3s1Wg7lDdSAFydjdY3Luwu8NC2jd-obe6-Yi39lVbCAMa3R2qzZDvhoCh2IQAvD_BwE) to SwaggperHub then you can create an API with just specifing the OpenAPI version and the name of the API then Sagger will open an editor page generating a sample OpenAPI specifications.  
Now you can Start Editing this sample to describe your API to see live visualisation of what you write is automatically rendered at the UI on the right side of the editor page.  

## OpenAPI content
____
first line of the document Always specifies which version of OpenAPI you are using, will be as follows  
```yaml
openapi: 3.0.0
```
then you will find "servers" which describes servers you are using as follows
```yaml
servers:
  - description: SwaggerHub API Auto Mocking
    url: https://virtserver.swaggerhub.com/<UserName>/<API Name>/1.0.0
```
Yaml uses spaces to deduce properties so properties which are verttically aligned are on the same level and belong to the first one above on higher level.   
Text which includes "-" before specifies an item of an array that means that "servers" is an array and "description" and "url" are properties describing the first element in that array.  
After "Server" you will find "info" which will contain general information about the API such as 
```yaml
info:
  description: This is a simple API
  version: "1.0.0"
  title: Simple Inventory API
  contact:
    email: you@your-company.com
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
```
Then there is "tags" which you use to map your logic as every endpoint will have a tag and those who have the same tage will be mapped under the topic of that tag.  
example:
```yaml
tags:
  - name: admins
    description: Secured Admin-only calls
  - name: developers
    description: Operations available to regular developers
```
After "tags" you will find "paths" which is where you are going to describe you endpoints and their operations.  
example:
```yaml
paths:
  /inventory:
    post:
      tags:
        - admins
      summary: adds an inventory item
      operationId: addInventory
      description: Adds an item to the system
      responses:
        '201':
          description: item created
        '400':
          description: 'invalid input, object invalid'
        '409':
          description: an existing item already exists
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InventoryItem'
        description: Inventory item to add
```
At the End of the document there is "components" which contains "schemas"
which describes the models you can use in your code as a reference without having to rewrite them every time.  
example:
```yaml
components:
  schemas:
    InventoryItem:
      type: object
      required:
        - id
        - name
        - manufacturer
        - releaseDate
      properties:
        id:
          type: string
          format: uuid
          example: d290f1ee-6c54-4b01-90e6-d701748f0851
        name:
          type: string
          example: Widget Adapter
        releaseDate:
          type: string
          format: int32
          example: '2016-08-29T09:12:33.001Z'
```
___
##  Paths Components!
paths contain the main descriptoin to your endpoints and their functions and actions.  
paths are divided into endpoints and inside every endpoit you describe its actions   
example:
```yaml
/app:
      post:
        tags:
          - "App"
        description: "Create app"
        operationId: 'createApp'
        requestBody:
            required: true
            content:
              application/json:
                schema:
                  properties:
                    name:
                      type: string
                    extraData:
                      type: string
              application/xml:
                schema:
                  properties:
                    name:
                      type: string
                    extraData:
                      type: string
        responses:
          201:
            description: App created successfully
          409:
            description: error
```
that is an example of an endpoint "app" which contains one action (post).  
first layer after "paths:" will be describbing the endpoint's name as "/app" that means that this endpoint is called when sending a request to url of the server followed by (/app).  
then the next layer will be the type of the request of the operation which will be one of (Post,Get,Put,Delete). the end poin from the path and the type of the request your api is receiving is what determines which operation is it going to excute.  
in this case the request type it will be recieving to call this operation is "post"  
   
first three attributes of this operation are (tags,descripton & operationId) and they are all for the mapping of the program as the operations having the same "tags" are mapped together under the same topic
also description is for labeling or refering for extra info about the operation.  
then we have "requestBody" that describes the parameters or the properties that are sent in the body of the request of this operation.  
Then there is "parameters" which describes parameters sent in other places than body such as header, cookie or path.  
Finally there is "responses" which describes the responses which can be returned in this operation and usually titled with [statuse code](https://www.restapitutorial.com/httpstatuscodes.html)
___
### Using Refrences
when you try to call any thing as a reference you use "ref" property
which always start with ($ref:) then the path which will always start with '#'  
as follows:
```yaml
    $ref: '#/components/schemas/user'
```
in this example we are calling a model called user in schemas in components.  
___
### Using Path Parameters
when putting a variable in path you define it in the endpoint like:  
```yaml
  /user/{userId}:
 ```
you will have to define this variable (userId in this case) in the parameters in each operation in this end point cuh as
```yaml
 /user/{userId}:
      put:
        tags:
          - "User"
        description: "Edit User"
        operationId: "editUser"
        parameters: 
          - in: path
            name: "userId"
            schema:
              type: string
            required: true
```
___
### Using Authorization
  you can define components you use it as an authorization that are sent in headers of every requests by defigning the types of objects you are sending in "security"   
  example:
  ```yaml
  security:
  - ApiKeyAuth: []
  - OAuth2:
      - scope1
      - scope2
  - OpenId:
      - scopeA
      - scopeB
  - BasicAuth: []
  - jwtAuth: []
  ```
  then you describe the objects in "Components/securitySchemas"
  like:
  ```yaml
  components:
    securitySchemes:
      BasicAuth:
        type: http
        scheme: basic
      BearerAuth:
        type: http
        scheme: bearer
      ApiKeyAuth:
        type: apiKey
        in: header
        name: X-API-Key
      OpenID:
        type: openIdConnect
        openIdConnectUrl: https://example.com/.well-known/openid-configuration
      OAuth2:
        type: oauth2
        flows:
          authorizationCode:
            authorizationUrl: https://example.com/oauth/authorize
            tokenUrl: https://example.com/oauth/token
           scopes:
              read: Grants read access
              write: Grants write access
              admin: Grants access to admin operations
```
___