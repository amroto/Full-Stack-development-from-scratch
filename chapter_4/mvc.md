# **MVC** {MVC} 
MVC is one of the most commonly used design patterns.  It was designed to easily implement complex GUI application.  
It was first introduced to the world in 1979, when the concept of GUI programming began to rise, which is way before the appearance of web applications.  
MVC promotes the cohesion(classes that represent or defines one type of object) and aims to lower the connection between these classes. In oop, it's considered a bad practice to mix between the data presentation and data code. So MVC separate them by introducing a controller class that communicates between presentation and data classes.  

## **MVC in web application**
With the rise of web application design patterns started to be more of a necessity, hence, MVC was introduced to the web. Its usage became popular, and it was the implemented for many programming languages as Ruby, Python, and C#. The pattern we use in the web application is an adaptation of the original pattern to fit the concept of application design to the web.   
In the early days of the web, MVC was implemented on the server side, with the client requesting updates via links. However, nowadays more logic is pushed to the client side. 

## **Model-View-Controller**
The MVC is divided into three component as mentioned in its name. Each component has different functionality. 

![MVC](https://3.imimg.com/data3/TH/DQ/MY-9191833/mvc-web-development-500x500.jpg)

### **1. Controller**
The controller is the intermediate class between the model and the view classes. First, it manages the HTTP requests that the view sends. Then, it translates these requests to suitable actions the model should take. Finally, it selects the suitable view to handle the response.

### **2. Model**
The model is responsible for the data that the application should contain and how to manage it. It contains ways to query or update the data. The most commonly used database operations are: create, read, update and delete. Any change in the data the model will notify the controller which take the suitable view to show the response. 

### **3. View**
The view is responsible for presenting data that is coming from the model, and it defines how the application should be displayed in the browser.

As a web developer, you have probably used MVC even if you have never implemented consciously. Your model is a sort of database. Your controller is probably written in javascript or HTML controlling the incoming requests and handling the responses. Your view is the frontend you write with whatever language you like.

## **Why MVC**
### **1. Separation of presentation and logic**
Due to the presence of the controller class as an intermediate class between the model and the view, it led to the separation between the requests made and selecting the suitable view. There is no communication between the request made by the user and the view presented.
### **2. Reduction of code complexity and increasing the modularity**  
Since the code is divided into three classes, hence, the large application became less complex to implement, it became easier to maintain and test. It also made it easier for the team to develop, allowing more than one team member to work in the same phase without code conflict. It also made it easier to add functionality to the classes that would have taken time to design and test.