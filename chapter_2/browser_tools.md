## Browser Tools {#browser-tools}

### Devtools {#devtools}
Chrome DevTools is a set of web developer tools built directly into the Google Chrome browser. DevTools can help you diagnose problems quickly, which ultimately helps you build better websites, faster.
You can open it by clicking ```Ctrl+shift+i``` or ```F12```.
It allows you to:
* View and change CSS and see the changes immediately 
* Debug JavaScript
* Execute lines of JavaScript code in realtime, and even change values of variables during execution.
* View all errors from the **Console** tab.
* Get quantitative reports of your site speed, as well as tips on how to improve it, all in the **Audit** tab.

### Chrome extensions {#chrome-extensions}