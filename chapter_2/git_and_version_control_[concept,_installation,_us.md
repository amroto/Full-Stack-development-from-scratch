#Github and version control
Git is a Version Control System (VCS) developed by **Linus Torvalds** - Creator of Linux - in 2005. It since then became the most widely used VCS.

**Version control system** is a system that manages changes to mainly documents and computer programs. Changes are identified by a code, and are associated with a timestamp and the person making the changes. After making each change a new revision is created. Revisions can be compared, restored or even merged.

##Advantages of using Version Control {#advantage_of_git}

1. Allows you to revert to a previous revision (Called commit in case of git) if something goes wrong.
1. Multiple team members can work on different features.
1. Multiple team members can also work on the same file and git can merge those file to get one file. 
1. setup a Push to Deploy can save you tons of time.

##How to use Git
Git can be used from a GUI or from the command line. However for the purpose of this manual we are going to use the command line. 
After you install git, open the command line and browse to the location that you'll start this project in.
To start working with git you will either create a new repository on your PC or clone an existing remote repository (on github.com for example).
```git init``` initializes a repository in the folder where you are currently in.
```git clone URL``` Creates a clone of the repository.

Now that we have a local repository, let's add a new file - "CoolFile.txt" -
To see if git detects the changes that we made. We will use the command ```git status```
```git status```  displays the state of the working directory and the **staging area**. It lets you see which changes have been staged, which haven’t, and which files aren’t being tracked by Git. 
**So what's a staging area ?**
In English it means "A stopping place or assembly point". In Git it's the 'place' where you move the files ready to be **committed** (To make a revision containing the staged files).
So to commit our "CoolFile.txt", we need to first move it to the staging area using the command ```git add```. We can do that in two ways.
```git add .``` where . means add all changed files to the staging area.
```git add CoolFile.txt``` This will move this file only to the staging area.

The fastest way to commit the changes and make our first commit is to use the command ```git commit -m "OUR COOL MESSAGE"``` where -m means that what follows is a message surrounded by two quotation marks.
Git allows you to write a short message explaining what was changed in each commit. Make sure you use it wisely.

Now we want to **push** these changes (commits) so our teammates can see them.
Before we do this we need to let git know where is our remote repository; ie: give it the URL. If you cloned this repository you can skip this step because Git knows the URL already, and just use ```git push```
```git remote set-url origin URL``` this will set the remote repository URL

Below is the general form to use push
```git push  <REMOTENAME> <BRANCHNAME>``` Branch name is "master" so far, as we haven't discussed branches yet. Remote Name can be the URL or "origin" which Git understands it's the main URL -Usually the one you cloned the repo from-

So what if your teammate pushed a commit as well to your coolGitRepository.com. Now you need to **pull** it so you can have it locally on your pc. You can do this by running the command ```git pull```
However below is the general form to pull
```git pull <REMOTENAME> <BRANCHNAME>``` The same rules apply as in pushing.

As we have finsihed the very basics of Git. Now is a good time to pat ourselves on the back and discuss some **Git tips & tricks**

##Git tips & Tricks

* A good rule of thumb is to make one commit per logical change.
* Make sure you pull before you commit. Because you must have all the commits on the remote repo before you can push, otherwise you will have to pull and make a new commit which is very annoying.
* If you made some changes and then changed your mind and wanted to go back to the last commit use ```git reset --hard```
* Use the imperative mood in your commit messages. Ex: "add the coolest feature in history". **Notice "add" not "added"**

##Branching

![branch](./assets/branch.png)

 Branching is a feature available in most modern version control systems. However it's more efficient in Git than in other systems. A branch represents an independent line of development you can think of them as a way to request a brand new working directory, staging area, and project history. New commits are recorded in the history for the current branch, which results in a fork in the history of the project. It's a good practice to have your working version of the application on the master branch, and make a new branch to add a new feature on it without affecting the master branch and merge this branch with the master branch when you are done with this feature.
 The master branch can also be locked by the owner or the senior member of the team so he/she can review changes before it can be merged to the master.

Now that you are familiar with git, we can just list the most important commands and what they do.

```git branch``` shows the current branch. (so far we are sill on master)
```git branch branch-name``` create a new branch with this name
```git branch -d branch-name``` delete the specified branch
```git merge branch-name``` merge this branch into the current branch.
**CAUTION: YOU HAVE TO MAKE SURE THAT YOU ARE CURRENTLY ON THE REQUIRED BRANCH AND THAT YOU PULL THE LATEST CHANGES BEFOREHAND** 